For implementation automation tests was used Ruby and Cucumber as more understandable (possible change to RSpec in short period of time) , Watir Webdriver + PageObject pattern. Tests could be run on Firefox browser.

Requirements

    ruby 2.0.0-p481 or higher
    for installing all necessary gems - "bundle install" in project root folder
    browser - Firefox, Chrome

For running test navigate to project root folder and type command "cucumber --format PrettyFace::Formatter::Html --out index.html"
Open reports - index.html