class SearchPage
  include PageObject
  include IssueMenu

  spans :issue_text, class: 'issue-link-summary'
  text_field :search_field, id: 'searcher-query'
  button :search, class: 'search-button'
  link :edit_issue, css: '.toolbar-trigger.issueaction-edit-issue'

  def get_issue_summary
    issue_text_elements.sample.text
  end

  def search_issue_by_name(name)
    self.search_field = name
    search
  end

  def searched_issue_name
    wait_until {issue_summary_element.visible?}
    issue_summary
  end

  def edit_created_issue
    wait_until {edit_issue_element.visible? }
    edit_issue
  end

end