module TopSection
  include PageObject

  link :issues, id: 'find_link'
  link :search_for_issue, id: 'issues_new_search_link_lnk'
  link :profile, id: 'header-details-user-fullname'
  link :log_in, class: 'login-link'
  link :create_issue, id: 'create_link'

  def signed_in?
    profile_element.visible?
  end

  def open_search
    wait_until{issues_element.visible?}
    issues
    search_for_issue_element.when_visible.click
  end
end