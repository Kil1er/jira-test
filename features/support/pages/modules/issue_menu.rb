module IssueMenu
  include PageObject

  text_field :project_field, id: 'project-field'
  link :project_name, class: 'aui-list-item-link'
  text_field :issue_type, id: 'issuetype-field'
  link :type, class: 'aui-list-item-link'
  text_field :summary, id: 'summary'
  text_field :priority_field, id: 'priority-field'
  links :priority, css: '.aui-list-item-link.aui-iconised-link'
  text_field :env, id: 'environment'
  text_field :description, id: 'description'
  checkboxes :burger_option, css: '.checkbox:empty'
  select_list :animals_type_list, class: 'cascadingselect-parent'
  select_list :animal_list, class: 'cascadingselect-child'
  button :submit_issue, id: 'create-issue-submit'
  button :update_issue, id: 'edit-issue-submit'
  text_field :comment, id: 'comment'
  h1 :issue_summary, id: 'summary-val'

  def create_new_issue(issue_data)
    wait_until{ project_field_element.visible? }
    self.project_field = issue_data[:project]
    project_name_element.click
    wait_until{ issue_type_element.visible? }
    self.issue_type = issue_data[:type]
    type_element.click
    wait_until{ summary_element.visible? }
    self.summary = issue_data[:summary]
    priority_field_element.click
    priority_elements.sample.click
    self.env = issue_data[:env]
    self.description = issue_data[:description]
    check_burger_options
    animals_type_list_element.options.sample.select
    animals_type_list_element.click
    animal_list_element.options.sample.select
    animal_list_element.click
    submit_issue
  end

  def update_created_issue(issue_data)
    wait_until{ summary_element.visible? }
    self.summary = issue_data[:summary_updated]
    self.env = issue_data[:env_updated]
    self.description = issue_data[:description_updated]
    update_issue
  end

  def check_burger_options
    burger_option_elements.each { | option | option.check }
  end
end