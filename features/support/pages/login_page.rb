class LoginPage
  include PageObject

  text_field :username, id: 'username'
  text_field :password, id: 'password'
  button :submit_login, id: 'login-submit'
  checkbox :stay_logged, id: 'rememberMe'

  def sign_in(credentials)
    wait_until {username_element.visible?}
    self.username = credentials[:username]
    self.password = credentials[:password]
    check_stay_logged unless stay_logged_checked?
    submit_login
  end

end