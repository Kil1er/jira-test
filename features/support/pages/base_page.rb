class BasePage
  include PageObject
  include TopSection
  include IssueMenu

  link :issue_created, class: 'issue-created-key'
  link :issue_updated, class: 'aui-message-success'
  div :issue_dialog, id: 'create-issue-dialog'

  page_url 'https://jira.atlassian.com'

  def issue_created_success_message_appears?
    wait_until{ !issue_dialog_element.visible?  }
    issue_created_element.visible?
  end

  def issue_updated?
    wait_until{ !issue_dialog_element.visible? }
    issue_summary_element.when_visible.wait_until{ !issue_updated_element.visible? }
    issue_summary.include?(data_load[:issue][:summary_updated])
  end

end