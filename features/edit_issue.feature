Feature: Edit issue
  In order to correct mistakes
  As a user
  I want to edit issues

  Background: Login
    Given I open BasePage
    When I sign in

  Scenario: Edit issue
    When I search for created issue
    And I edit issue
    Then I should see that issue updated