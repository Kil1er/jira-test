When /^I sign in$/ do
  on(BasePage).log_in
  on(LoginPage).sign_in(data_load[:credentials])
end

Then /^I should see profile menu on page$/ do
  expect(on(BasePage).signed_in?).to eq true
end