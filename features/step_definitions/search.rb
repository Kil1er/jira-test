When /^I search for (random|created) issue$/ do |issue|
  on(BasePage).open_search
  @name_of_issue = data_load[:issue][:summary]
  @name_of_issue = on(SearchPage).get_issue_summary if issue == 'random'
  on(SearchPage).search_issue_by_name(@name_of_issue)
end

Then /^I should see searched issue$/ do
  expect(on(SearchPage).searched_issue_name.downcase).to include @name_of_issue.downcase
end