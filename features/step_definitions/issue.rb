When /^I click on create issue button$/ do
  on(BasePage).create_issue
end

When /^Create new issue$/ do
  on(BasePage).create_new_issue(data_load[:issue])
end

When /^I edit issue$/ do
  on(SearchPage).edit_created_issue
  on(BasePage).update_created_issue((data_load[:issue]))
end

Then /^I should see that issue (created|updated)$/ do |status|
  expect(on(BasePage).issue_created_success_message_appears?).to eq true if status == "created"
  expect(on(BasePage).issue_updated?).to eq true if status == "updated"
end