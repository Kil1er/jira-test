Feature: Creation of new issue
  In order to document bugs
  As a User
  I should have possibility to create new issues

  Background: Log in
    Given I open BasePage
    And I sign in

  Scenario: Create new issue
    When I click on create issue button
    And Create new issue
    Then I should see that issue created